// CRUD operation

// Insert Document(create)


/*
Syntax: 
	insert One Document
		db.collectionName.insertOne({
			"fieldA" : valueA",
			"FieldB" : valueB"
		})

	Insert many documents

		db.collectionName.insertMAny([
		{
			"fieldA" : valueA",
			"FieldB" : valueB"
		},

		{
	
			"fieldA" : valueA",
			"FieldB" : valueB"
		}

		])


*/


db.users.insertOne({
	"firstNme":"Jane",
	"lastName": "DOe",
	"age": 21,
	"email": "jane@gmail.com",
	"company": "none"
})

db.users.insertMany([
{
	"firstNme":"Stephen",
	"lastName": "Hawkin",
	"age": 76,
	"email": "stephen@gmail.com",
	"company": "none"
},

{
	"firstNme":"Neil",
	"lastName": "Armstrong",
	"age": 82,
	"email": "neil@gmail.com",
	"company": "none"
}

])

db.courses.insertMany([
{
	"name" : "Javascript 101",
	"price": 5000,
	"description": "Introduction to Javascript",
	"isActive": true
},
{
	"name" : "HTML 101",
	"price": 2000,
	"description": "Introduction to HTML",
	"isActive": true
},
{
	"name" : "CSS 101",
	"price": 2500,
	"description": "Introduction to CSS",
	"isActive": true
}

])

// Find DOcuments(Read/Retrieve)

db.users.find()

db.users.find({
	"firstName" :"Jane"
}) 

db.users.find({
	"firstName" :"Jane"
	"age": 82

}) 

db.users.findOne({}); // returns the first documents in our collection

db.users.findOne({
	"firstName": "Stephen"
})


// Update Documents

db.users.insertOne({
	"firstNme":"Test",
	"lastName": "Test",
	"age": 0,
	"email": "test@gmail.com",
	"company": "none"
})

// Updating one document

db.users.updateOne(
{
	"firstName": "Test"
},
{
	$set:{

			"firstName":"Bill",
			"lastName": "Gates",
			"age": 65,
			"email": "billgates@gmail.com",
			"department": "Operation",
			"status": "active"

	}
}
);

// Removing a field

db.users.updateOne(
{
	"firstNme": "Bill"

},
{
	$unset: {
		"status":"active"
	}
}


)

db.users.updateMany(
{
	"company": "none"

},

{
	$set:{
		"company": "HR"
	}
}
)



db.users.updateOne(
	{},
{
	$set:{
		"company": "operation"
	}
}


)


db.users.updateMany(
	{},
{
	$set:{
		"company": "operation"
	}
}


)

// Deleting Documents(Delete)

db.users.insertOne({
	"firstNme": "Test"

});

db.users.deleteOne({
	"firstNme": "Test"
});

db.users.deleteMany({
	"company":"comp"
});


db.courses.deleteMany({})