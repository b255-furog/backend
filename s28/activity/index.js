db.room.insertOne({
	"name":"single",
	"accomodates": 2,
	"price": 1000,
	"description": "A simple room with all basic neccessities",
	"room_available": 10,
	"isAvailable": false
})

db.room.insertMany([
{
	"name":"double",
	"accomodates": 3,
	"price": 2000,
	"description": "A room fit for a small family going on a vacation",
	"room_available": 5,
	"isAvailable": false
},
{
	"name":"queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with queen sized bed perfect for a simple getaway",
	"room_available": 15,
	"isAvailable": false	
}
])

db.room.find({
	"nameame" :"double"

});

db.room.updateOne(
{
	"name": "queen"

},
{
	$set:{
	"name":"queen",
	"accomodates": 4,
	"price": 4000,
	"description": "A room with queen sized bed perfect for a simple getaway",
	"room_available": 0,
	"isAvailable": false	
	}
}


);

db.room.deleteMany({
	"room_available": 0
})