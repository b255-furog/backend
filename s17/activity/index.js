console.log("hello world")
/*
	//Note: strictly follow the variable names and function names.

	1. Create a function named printUserInfo() which is able to display a user's to fullname, age, location and other information.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
*/
	//first function here:
	function printUserInfo(){
		let fullName = " I am Edison Furog.";
		let age = " I am 29 years old.";
		let address = " I live in Cotcot, liloan, Cebu.";
		let fatherInfo = " I am a father of 1.";
		let kidGender = " He is a cute little boy.";

		console.log(" This is the info about me");
		console.log(fullName);
		console.log(age);
		console.log(address);
		console.log(fatherInfo);
		console.log(kidGender);
	}

	printUserInfo()


/*
	2. Create a function named printFiveBands which is able to display 5 bands/musical artists.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	//second function here:
	function printFiveBands(){
		let firtsBand = " 1. Cueshe.";
		let secondBand = " 2. Six Cycle Mind.";
		let thirdBand = " 3. Eraserheads.";
		let fourthBand = " 4. Moonstar88.";
		let fifthBand = " 5. December Avenue.";

		console.log(" This are my favorite band");
		console.log(firtsBand);
		console.log(secondBand);
		console.log(thirdBand);
		console.log(fourthBand);
		console.log(fifthBand);
	}

	printFiveBands()


/*
	3. Create a function named printFiveMovies which is able to display the name of 5 movies.
		-invoke the function to display information similar to the expected output in the console.
		-check your spelling
	
*/
	
	//third function here:
	function printFiveMovies(){
		let movie1 = " 1.The Day After Tomorrow.";
		let movie2 = " 2. Kingsman: Golden Circle.";
		let movie3 = " 3. Pirates of the Carribean.";
		let movie4 = " 4. Sully.";
		let movie5 = " 5. Uncharted";

		console.log(" This are my favorite movies");
		console.log(movie1);
		console.log(movie2);
		console.log(movie3);
		console.log(movie4);
		console.log(movie5);
	}

	printFiveMovies()



/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
		-check your spelling

		-invoke the function to display information similar to the expected output in the console.
*/

function printFriends(){
	let friend1 = "Eugene"; 
	let friend2 = "Dennis"; 
	let friend3 = "Vincent";

	console.log("These are my friends:");
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends()


function signToProceed(){

	let firstName = prompt("Enter your First Name");
	let sureName= prompt(" Enter your Surename");
	console.log("Hello "+firstName + " " + sureName);
	console.log("Welcome to my page");
}
signToProceed()








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		printUserInfo,
		printFiveBands,
		printFiveMovies,
		printFriends
	}
} catch(err){

}