console.log("hello world");
// functions
	// functions in javascript are lines/block of codes that tell our device to perform a certain task
	// functiona re mostly created to create complicated task to run several lines of code in succession
	// Function declaration


	function printName(){
		console.log("My name is john");
	};

	// function invocation

	printName();

	// function declartion vs expression

	// function declartaion

	// a function can be created through fiucntion declaration by using the function keyword and adding a function name

	function declaredFunction(){
		console.log("hello world from declaredFunction")
	}
	declaredFunction();

	// function expression
	// a function can also be stored in a cvariable it is called a function expression
	// a function expression is an anynomous fucntion assigned to the variableFunction

	// anonymous function is function without a name


	let variableFunction = function(){
		console.log("hello again")
	}

		variableFunction();

		// we can also create a function exprerssion of a named fucntion
		// however to invoked the function expression, we invoke it by its variable name, not by its function name

		let funcExpression= function funcName(){
			console.log("hello word from the other side")
		}
	funcExpression()
	// you can reassign declared fiunctions and functions expression to new anynomous functions


	declaredFunction=function(){
		console.log('updated declaredFunction')
	}
	declaredFunction()

	funcExpression=function(){
		console.log("updated funcExpression")
	}

	funcExpression()


	// however we cannot reassigned a fucntion expression initiazed by a const

	const constantFunc=function(){
console.log("initialized with const")
	}
	constantFunc()


// function scoping

/*
scope is the accessibilty(visibility) of variables within our program

Javascript variables has 3 types of scope:
1.
2
3
*/
 {

 	let locavVar = 'Armando perez'
 }

 let globalVar="Mr.Worlwide"

 console.log(globalVar)
// 

// function scope

function showNames(){
	var functionVar = "Joe"
	const functionConst="john"
	let functionLet ="jane"

	console.log(functionVar)
	console.log(functionConst)
	console.log(functionLet)
}
showNames()

// nested fucntion

	// you can create another function insie a function. tis is claeed a nested function

	function myNewFunction(){
		let name="jane"

		function nestedFunction(){
			let nestedName="john"
			console.log(name)
		}
		nestedFunction()
	}
	myNewFunction()

// function and global scope variable
	// globlal scope variable

	let globalName="alexandro"

	function myNewFunction2(){
		let nameInside ="rens"

		console.log(globalName)
	}

	myNewFunction2()

// using alert
	// alert allows us to show a small window at the top of our page to show information to our users. as opposed to console log()

	alert("hello world")

	function showSampleAlert(){
		alert("hello,User");
	}
	showSampleAlert();

	console.log( " i will only log in the console when the alert is dismissed")

	// using prompt
	// prompt allow us to show a small window ath the browser to gather user input. it, much like alert.
	/*will have the page wait until it is dismissed*/

	let samplePrompt= prompt("enter your name")

	console.log('hello,' + samplePrompt)

	let sampleNullPrompt=prompt("dont enter anythinng")

	console.log(sampleNullPrompt)

	// return an empty string when there is no input
	// or null if

	function printWelcomeMessage(){ 
		let firstname =prompt("enter your first nname")
		let lastname = prompt("enter last name")

		console.log("hello" + firstname + " " + lastname)
		console.log("welcome to my page")

}
printWelcomeMessage()

// funstion naming convertions
// function names should be definitive of the task it will perform. it usually ciontains a verb

function getCourses(){
	let courses = ["science","math", "english"]
	console.log(courses)
}
getCourses()

// avoid generic names to avoid confucion

function get(){
	let name="jamie"
	console.log(name)
}
get()
// avoid pointless and inapproriate function names

function foo(){
	console.log(25%5)
}
foo()

// name yyour functions in small caps. follow camelCase when naming and functions

function displayCarInfo(){
	console.log("Brabd:toyata")
	console.log("sedan")
	console.log("1500000")
}
displayCarInfo()