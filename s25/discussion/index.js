console.log('hello world')

// [section] json object
/*
jaosn stands for javascripot object notaion
-json is also use in other programming languages hence the name javascript object notattion
-core javacsripit as a bulit in json object that contains method for parseing json objects and converting strings to JS objects
-js objetcs are not to be confused with json
-serialization is the proicess of converting data into a seris of bytes fro easier tranmission /transfer of information
-a byte is a unit of data that is eight binanry diguits(1 and 0 ) thatis used to rerprresent a chararter(letters,numbers, or topographic symbols)
-bytes are infor,ation that a computer process to perform different task
*/
// JSON Objects:
/*{
	'city': 'quezon city'
	'provinve:'"metro manila"
	"country:""Phili"
}*/

// JSON ARRAYs
/*
"cities" : [
{"city": "quezon city", "province": "metro manila", "country": "phil"},
{"city": "manila city",  "province": "metro manila", "country": "phil"},
{"city": "makaiti city",  "province": "metro manila", "country": "phil"}
]
*/
// [Section] Json Method
/*
-the json object contaions methods for parsing and converting data into stringfield JSON

*/
// [Section] Converting Data into stringified JSON

/*
-stringified JSON is JS object converted innto a string to be used on other functions of a jS application
-they are commonly used in HTTP request where informatiom is required to be sewnt and received in a stringified JSON fromat
-request are an important part of programming where an application communicates with a backend application to perfoms different task such as getting/creating dta into a database
-a frontend application is an application used to inteact with the users to perfrom difeerent visual task and display infor,ation while backend application are commonly used for all business logic and data processing.
-a database  normally stores information as data that canbe sused iun most applications
-commonly stored data in data bases are user information, trransaction record an d product informationm
-node/express js are 2 ttypes ogf technologies that are used in creating backend application which proceses request from the frontend application
*/

 let batchesARR = [{batchName: 'Batch X'}, {batchName:'Batch Y'}]
 console.log('result from strigify method');
 console.log(JSON.stringify(batchesARR))

 let data = JSON.stringify({
 	name: 'john',
 	age: 21,
 	address: {
 		city: 'manila',
 		country:'Philippines'
 	}
 })
 console.log(data)

 // [section] using strigify method with variable
 /*
-when inromation is sotored in a variable and is not hard coded into an objectthat is being stringified, we can supply the vale with a varibale
-the"property name and "value" would have the same name which can be confusing for begiiners
-this is done on purpose fro code reaability meaning when an information is sotored in a variable and when the obkject created to be stringified is created, we supply the variable name instead of hard coded value
-thisd is commonly used when the information to be stored and sent to a backend application will come from a frontend application
 */

/* let firstName = prompt('whats is your first name')
 let lastName = prompt('whats is your last name')
 let age = prompt('whats is your age')
 let address = {
 	city : prompt('what is your address'),
 	country : prompt('what is your country')
 };
let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})

console.log(otherData)
*/
// [section] converting stringified JASON into JS Objects
/*
-objects are common data types used in application because of the complex data struture that can be created out of them
-information is commonly sent to application in stringified JSON and then converted back into objects
-this happens both for sending information to a backend application
*/
let batchesJSON ='[{"batchName":"Batch X", "batchName2":"Batch Z"}, {"batchName": "Batch Y"}]';
let batchesOne = JSON.parse(batchesJSON)
console.log(batchesOne)
/*console.log('result from the parse method');
console.log(JSON.parse(batchesJSON));*/
let users ='[{"id": "1", "name": "Leanne Graham", "username": "Bret","email": "Sincere@april.biz", "address": {"street": "Kulas Light","suite": "Apt. 556","city": "Gwenborough","zipcode": "92998-3874","geo": {"lat": "-37.3159","lng": "81.1496"}},"phone": "1-770-736-8031 x56442", "website": "hildegard.org","company": {"name": "Romaguera-Crona", "catchPhrase": "Multi-layered client-server neural-net","bs": "harness real-time e-markets"}},{"id": "2","name": "Ervin Howell",   "username": "Antonette","email": "Shanna@melissa.tv","address": {"street": "Victor Plains","suite": "Suite 879","city": "Wisokyburgh","zipcode": "90566-7771","geo": {    "lat": "-43.9509",    "lng": "-34.4618"       }      },      "phone": "010-692-6593 x09125",      "website": "anastasia.net",      "company": {        "name": "Deckow-Crist",        "catchPhrase": "Proactive didactic contingency",        "bs": "synergize scalable supply-chains"      }},{  "id": "3",  "name": "Clementine Bauch",  "username": "Samantha",  "email": "Nathan@yesenia.net",  "address": {    "street": "Douglas Extension",    "suite": "Suite 847",    "city": "McKenziehaven",    "zipcode": "59590-4157",    "geo": {      "lat": "-68.6102",      "lng": "-47.0653"    }  },  "phone": "1-463-123-4447",  "website": "ramiro.info",  "company":{    "name": "Romaguera-Jacobson",    "catchPhrase": "Face to face bifurcated interface",    "bs": "e-enable strategic applications" }},    {"id": "4",  "name": "Patricia Lebsack",  "username": "Karianne",  "email": "Julianne.OConner@kory.org", "address":{ "street": "Hoeger Mall",    "suite": "Apt. 692",    "city": "South Elvis",    "zipcode": "53919-4257",    "geo": {"lat": "29.4572",      "lng": "-164.2990"}},  "phone": "493-170-9623 x156",  "website": "kale.biz",  "company": {    "name": "Robel-Corkery",    "catchPhrase": "Multi-tiered zero tolerance productivity",    "bs": "transition cutting-edge web services"  }}, {      "id": "5",      "name": "Chelsey Dietrich",      "username": "Kamren",      "email": "Lucio_Hettinger@annie.ca",      "address": {        "street": "Skiles Walks",        "suite": "Suite 351",        "city": "Roscoeview",        "zipcode": "33263",        "geo": {          "lat": "-31.8129",          "lng": "62.5342"        }      },      "phone": "(254)954-1289",      "website": "demarco.info",      "company": {      "name": "Keebler LLC",        "catchPhrase": "User-centric fault-tolerant solution",        "bs": "revolutionize end-to-end systems"     }}, {      "id": "6",      "name": "Mrs. Dennis Schulist",      "username": "Leopoldo_Corkery",      "email": "Karley_Dach@jasper.info",      "address": {        "street": "Norberto Crossing",        "suite": "Apt. 950",        "city": "South Christy",        "zipcode": "23505-1337",        "geo": {          "lat": "-71.4197",          "lng": "71.7478"        }      },      "phone": "1-477-935-8478 x6430",      "website": "ola.org",      "company": {        "name": "Considine-Lockman",        "catchPhrase": "Synchronised bottom-line interface",        "bs": "e-enable innovative applications"      }}, {      "id": "7",      "name": "Kurtis Weissnat",      "username": "Elwyn.Skiles",      "email": "Telly.Hoeger@billy.biz",      "address": {        "street": "Rex Trail",        "suite": "Suite 280",        "city": "Howemouth",        "zipcode": "58804-1099",        "geo":{          "lat": "24.8918",          "lng": "21.8984"        }      },      "phone": "210.067.6132",      "website": "elvis.io",      "company": {        "name": "Johns Group",        "catchPhrase": "Configurable multimedia task-force",        "bs": "generate enterprise e-tailers"      } }, {      "id": "8",      "name": "Nicholas Runolfsdottir V",      "username": "Maxime_Nienow",      "email": "Sherwood@rosamond.me",      "address": {        "street": "Ellsworth Summit",        "suite": "Suite 729",        "city": "Aliyaview",        "zipcode": "45169",        "geo": {          "lat": "-14.3990",          "lng": "-120.7677"        }      },      "phone": "586.493.6943 x140",      "website": "jacynthe.com",      "company": {        "name": "Abernathy Group",        "catchPhrase": "Implemented secondary concept",        "bs": "e-enable extensible e-tailers"      }}, {    "id": "9",      "name": "Glenna Reichert",      "username": "Delphine",      "email": "Chaim_McDermott@dana.io",      "address": {        "street": "Dayna Park",        "suite": "Suite 449",        "city": "Bartholomebury",        "zipcode": "76495-3109",        "geo": {          "lat": "24.6463",          "lng": "-168.8889"        }      },      "phone": "(775)976-6794 x41206",      "website": "conrad.com",      "company": {        "name": "Yost and Sons",        "catchPhrase": "Switchable contextually-based project",        "bs": "aggregate real-time technologies"      }}, {      "id": "10",      "name": "Clementina DuBuque",      "username": "Moriah.Stanton",      "email": "Rey.Padberg@karina.biz",      "address": {        "street": "Kattie Turnpike",        "suite": "Suite 198",        "city": "Lebsackbury",        "zipcode": "31428-2261",        "geo": {          "lat": "-38.2386",          "lng": "57.2232"        }      },      "phone": "024-648-3804",      "website": "ambrose.net",      "company": {        "name": "Hoeger LLC",        "catchPhrase": "Centralized empowering task-force",        "bs": "target end-to-end models"      }}]'

let parsedUser = JSON.parse(users)
console.log(parsedUser)
