// Create Documents to use for our discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);

// [section]-Mongo DB Aggregation
/*
	-used to generate manipulated data and perform operations to create filtered that helps in analyzing data
	-compared to crud operation on our previous sessions, aggregation gives us access to manipulate,filter and compute results providing us with information to make necessary development decisions without having to create a frontend application.

*/

// Using the aggregate method

// -the $ symbol will refer to a field name that is availbale in the dosuments that are being aggregated on

db.fruits.aggregate([
{$match:{onSale:true}},
{$group:{_id:"$supplier_id",total:{$sum: "$stock"}}}
]);


// Fiel Projection with aggregation
// $project can be used when aggregating data to include/exclude fields from the returned results


db.fruits.aggregate([
{$match:{onSale:true}},
{$group:{_id:"$supplier_id",total:{$sum: "$stock"}}}, {
	$project:{_id:0}}
]);


// Sorting aggregated results
/*
	-the $sort can be used to change the order of aggregated results
	-providing a value of -1 will sort the aggregated result in a reverse order
*/

db.fruits.aggregate([
{$match:{onSale:true}},
{$group:{_id:"$supplier_id",total:{$sum: "$stock"}}}, {
	$sort:{_total:-1}}
]);

// Aggregating result base on array fields
/*
	-the $unwind deconstruct an array fields from a collection/field with an array value to output a result for each element
*/
db.fruits.aggregate([
	{$unwind:"$origin"},
	{$group:{_id:"$origin",kinds:{$sum:1}}}

]);


// use a lot of schema in capstone 2 and dapat tapusin ang capstone 2 para hindi mahirapan sa sa capstone 3(fullstack) April(4/5) capstone 2. Express and postman will be used.
// meron discussion leading to capstone 2,,para makatulong, pwede emodified lang..


// [section] Guidlines on Schema Design
/*
-Schema designs/data modelling is an importand feaiture when creating database
-Monggo DB documents can be categorized into normalized and de-normalized/embedded data
-Normalized data refers to a data structures where documents are referred to each other using their IDs for the related pieces of information
-De-normalized data/embedded data refers to a data structure where related pieces of information is added to a document as embedded object
-Both data structures are common practice but each of them have their pros and cons
-Normalized data makes it easier to read information

*/

// One to One Relationship
// creates an ID and stores it in a variable owner for use in document creation

var owner = ObjectId();

db.owners.insert({
	_id: owner,
	name:"John Smith",
	contact:"0987654321"
});

// change "the owner_id" using the actual id in the previously created document

db.suppiers.insert({
	name:"ABC Fruits",
	contact: "123456789",
	owner_id: owner
});

// One-To-Few Relationship
db.suppliers.insert({
  name: "DEF Fruits",
  contact: "1234567890",
  addresses : [
    { street: "123 San Jose St", city: "Manila"},
    { street: "367 Gil Puyat", city: "Makati"}
  ]
});

// One-To-Many Relationship
var supplier = ObjectId();
var branch1 = ObjectId();
var branch2 = ObjectId();

db.suppliers.insert({
  _id: supplier,
  name: "GHI Fruits",
  contact: "1234567890",
  branches: [
    branch1
  ]
});

// Change the "<branch_id>" and "<supplier_id>" using the actual ids in the previously created document
db.branches.insert({
  _id: <branch_id>,
    name: "BF Homes",
    address: "123 Arcardio Santos St",
    city: "Paranaque",
    supplier_id: <supplier_id>
});

db.branches.insert(
  {
    _id: <branch_id>,
      name: "Rizal",
      address: "123 San Jose St",
      city: "Manila",
      supplier_id: <supplier_id>
  }
);