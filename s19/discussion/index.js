console.log("hello world")

// Conditonal statements allows us to comntrol  the floe of our program. It allows us to run a statement/instruction if a condition is met or run another separate instruction if otherwise

// section if else, else if, else statment

let numA = -1
 if(numA < 0){
 	console.log("Hello")
 }

 // the result of the expression added in the  ifs conditon must result to true, else, the statmenty inside if() will not run



 // console.log( numA<0); results to true a


numA=0;
if (numA < 0){
	console.log("hello again if Num is 0")
}

// it will not run because the expression now results to false

console.log(numA < 0)

let city ="New York"
if (city === "New York"){
	console.log("Welcome to new york")
}

// else if

/*
	execute a stztement if previous conditions are false and if specified conditon is tru
	-The else if cluase is optional andd can be added to capture additional conditions to change the flow of a program
*/

let numH =1

if(numA < 0){
	console.log("hello")
}else if (numH > 0){
	console.log("world")
}

// we were able to run else if() statement after we evaluated that the if condition that the if condition was failed..

// ifthe if conditon was passed and run we will no longer evaluate the else if and end the process there

numA = 1

if (numA > 0){
	console.log("hello")
}else if (numH > 0){
	console.log("world")
}
// else if() statement no longer run because the if statement 

city = "tokyo"

if (city === "newyork"){
	console.log("welcome to new york")
}else if(city==="tokyo"){
	console.log("welcome to tokyo")
}

// since we failed the condition for the first if() we went to the else if() and checked and passed that condition


// else statment

/*
	-execute a statement if all other cionditions are false
	-the esle statement is optional and can be added to capture any other result to change the flow of the program
*/

if (numA < 0){

	console.log("hello");
}else if(numH===0){
	console.log('world');
}else{
	console.log("Error! numA is not less than 0");
}


// else statments should only be added is there is a preceeding if condition. else statment by itself will noit work, however, if staments will worl even there is no else statement

// if, else if, else statements with function
/*
	-most of the times we would likely used if, el;se if, else statements with functions to control the flow of poru application
	-by iincluding them iside the functions, we can decide when certain conditons will be checked insted of execuring statements when the javcscript loads
	-the return statement can be utyilized with conditonal statements in combination with functions to change values to be used for other features
*/

let message = "no message"
console.log(message)

function determineTyphoonIntensity(windspeed){
	if(windspeed < 30){
		return "not a typhoon yet";
	}
	else if(windspeed <= 61) {
		return "tropical depression detected";
	}
	else if(windspeed >= 62 && windspeed <= 88){
		return "tropical storm detected"
	}
	else if (windspeed >= 89 || windspeed <= 117){
		return "typhoon detected"
		}
	}


message = determineTyphoonIntensity(110)
console.log(message)

// Mini Activity
// Create a fucnction with an if else statements inside
// CThe function should test if a number is an even number or and odd number
// console.log the output if it is an even number
// console .log the output if it an odd number
 

 


// Truthy examples

/*if the result of an expression in a condition results to atruthy value the conditon returns tru and the corresppnsing statement are executed
-expression are any unit of code that can be evalluated to a value
*/

if (true){
	console.log ('truthy')
}
if(1){
	console.log('truthy')
}


// falsy examples

if (false){
	console.log('falsy')
}
if (0){
	console.log('falsy')
}
if (undefined){
	console.log('falsy')
}
// [Section] conditonal ternary operator

/*
	-the conditional ternary operator takes in three operands

	1. condition
	2. expression to execute if the consditon is truthy
	3. expression to execute if the condition is falsy

	-can be used as an alternative to an "if else" statement
	-commonly used for single statement execution where the result consist only one line of code
*/
//  single statement execution

let ternaryResult = (1<18)? true : false;

console.log( " result of ternary operator: " + ternaryResult);
// multi statement execution

let name;

function isUnderAge(){
	name = 'jane'
	return 'You are under the age limit'
}

/*
	-input received from the prompt function is returned as a string data type
	-the "parseint" function converts the input received in to a number date type
	-NAN(not a number)
*/

let age = parseInt(prompt( "what is your age"));
console.log(age);
let legalAge = (age > 18) ? isOfLegalAge() :
isUnderAge();
console.log("result opf ternary operator in functions: " + legalAge + ', ' +name)

// [section] switch statements
/*
	-the switch statements evaluate an expression aand matches the expressionvalue to a case clause. the switch will then execute the statements associtaed with that case as well as statements in cases that follow the match

	-switch cases are considered as"loops" meaning it will compare the expression with each of the case values until a match is found

	-the "break" sattement is uded to terminate the current loop once a match has been found

*/

let day = prompt("what day of the week it is today")
	.toLowerCase();
	console.log(day)

switch (day){
	case 'monday':
		console.log("the color of the day is orange");
		break
	case 'tuesday':
		console.log("the color of the day is red");
		break
	case 'wednesday':
		console.log("the color of the day is yellow");
		break
	case 'thursday':
		console.log("the color of the day is green");
		break
	case 'friday':
		console.log("the color of the day is blue");
		break
	case 'saturday':
		console.log("the color of the day is indigo");
		break
	case 'sunday':
		console.log("the color of the day is violet");
		break
	default:
		console.log("Please input valid day");
		break;				
	}

	// [section] try-catch-finally statement

	/*
		-"try-catch" statements are commonly used for error handling
		-there are instances when the application returns an error/warning that is not necessarily an error in the context of our code
		-thexe errors are a result of an attemp of the programming language to help developers in creating effcient code
		-they are used to specify a response whenever an exception/error is received

	*/

	function showIntensityAlert(windspeed){
		try{
			alert(determineTyphoonIntensity(windspeed))

			// error/err are commonly used variable names used by developers for storing errors
		}
		catch (error) {

			// the typeof operator is used to check the data type  of a value/expression and returns a string value of what the  data type is
			console.log(typeof error)

			// catch errors within the try statement
			// in this case the error in an unknown function 'alerat' which does not exist in javascript
			// error.message is uded to access the information relating to an error object

			console.log( error.message)
		}finally{

			// continue execution of a code regardless of success and failure of code execution in the 'try ' block to handle/resolve errors
			alert("intensity updates will show the alert")


		}
	}
	showIntensityAlert(56)