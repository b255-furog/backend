console.log('hello world')
/*
	
//Note: strictly follow the variable names and function names from the instructions.

*/
function addNum(num1, num2){
	console.log("Displayed sum of " + num1 + " and " + num2);
	console.log(num1 + num2);
}
addNum( 15, 5);

function subNum(num3, num4){
	console.log( "Displayed Difference of " +num3 + " and " + num4);
	console.log(num3 - num4);
}
subNum(20, 5)

function multiplyNum(num5, num6){
	return (num5*num6)
}
let product = multiplyNum(10, 50)
console.log("The product of " +10 + " and " + 50)
console.log(product)

function divideNum(num5, num6){
	return (num5/num6)
}
let qoutient = divideNum(50, 10)
console.log("The qoutient of " +50 + " and " + 10)
console.log(qoutient)

function getCircleArea(radius, exponents){
	let circleArea = (radius ** exponents) * 3.14
	console.log(" The result of getting the area of a circle radius: " + radius)
	console.log(circleArea)
}
getCircleArea(15, 2)

function getAverage(num1, num2, num3, num4){
	let averageVar = (num1 + num2 +num3 + num4) / 4

	console.log(" The average of " + num1 + ", " + num2 + ", " + num3 + ", " + num4)
	console.log(averageVar)
}
getAverage(20, 40, 60, 80)

function checkIfPassed (num7, num8){
	let percentage = (num7/num8)
	let result = percentage > .75
	console.log (" Is " +num7+"/"+num8 + " is a passing score" )
	console.log(result)
}
checkIfPassed(38, 50)




//Do not modify
//For exporting to test.js
try {
	module.exports = {
		addNum,subNum,multiplyNum,divideNum,getCircleArea,getAverage,checkIfPassed
	}
} catch (err) {

}