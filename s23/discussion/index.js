console.log("hello world")

// [Section] Object

/*
an object is data type that is used to represent real world objects
-it is a collection of related data and or a functionalities
-in javascriot, most core javascript faetures like string and array are objects(String are a collection of charcaters and array are collection of data)
-information stored iun objects are represented in a 'Key:value' pair
-a 'key' is also m,ostly referred to as a 'property' of an object
-different data types may be stored in an objects property creating complex data structures

*/

let cellphone ={
	name: 'Nokia 3210',
	manufactureDate: 1999
} 
console.log('result from creating objects using initializer/literal notation')
console.log(cellphone)
console.log(typeof cellphone)
console.log(cellphone.name)
console.log(cellphone.manufactureDate)

// creating object using  a constructor function

/*
-create a reusable function to create several objects that have the same data structure
-This is useful for creating multiple instances or copies of an object
-an instance a concrete occurance of any object which emphasize on the distinct or unique identity of it
*/
// This is an object
// The 'this ' keyword allows to assign a new object properties by associating them with values received from a constructors function parameters
function Laptop(name, manufactureDate){
	this.name =name
	this.manufactureDate = manufactureDate
}

// The 'new' operator creates an instance of an object
let laptop = new Laptop ( 'Lenovo', 2008)
console.log('results from creating objects using object constructors')
console.log(laptop)

let myLaptop = new Laptop("macbook air", 2020)
console.log('results from creating objects using object constructors')
console.log(myLaptop)


// Creating empty objects
let computer = {}
let myComputer = new Object()

// [Section] acessing object properties


let array =[laptop,myLaptop]
// maybe confuced for accessing array indexes
console.log(array[0]['name'])
// this tella us that array[0] is an object by using the dor notattion
console.log(array[0].name)

// [section] intializing/adding/deleting/reassigning object properties
/*
-like any other variable in java scripot, object may have their properties iinitialize/added after the object was created/declared
-this is useful fo time when an objects properties are undetermined at the time of creating them
*/
let car ={}
// initializing/adding object properties using dot notation
car.name = 'honda civic'
console.log( 'results from adding properties using dor notation')
console.log(car)
console.log(car.name)

// initializing/adding object properties using square bracket notation
/*
-while using [] square bracket this will allow access to spaces when assigning property names to make it easier to read, this also makes it so that object properties can only be accessed using the square bracket nottaion
*/
car['manufacture Date'] = 2019
console.log( car['manufacture Date'])
console.log(car['manufacture Date'])
console.log([car.manufactureDate])
console.log( 'results from adding properties using square bracket notation')
console.log(car)

// deleting object properties

delete car['manufacture Date']
console.log( 'results from adding properties using delete properties')
console.log(car)

// reassighning object properties

car.name ='dodge charger r/t'
console.log( 'results from adding from reassigning properties')
console.log(car)

// [section] object methods

/*
-a method is function which is property of an object
-they are also a function and one of the differences they have is that method are functions related to a specific object
-methods are useful for creating objects speciifc functions which are used to perform tasks on them
*/
let person = {
	name:' john',
	talk: function(){
		console.log('hello my name is ' + this.name)
	}
}
console.log(person)
console.log('results from object methods')
person.talk()

// adding methods to object

person.walk = function(){
	console.log(this.name + ' walk 25 steps forward')
}
person.walk()

// mini activity
// add a method to the person object
// that method should be able console.log a hobby being done by the jhon
// used the "this" keyword to access the name john


// Methods are useful for creating resuable functions that perform task rel;ated objects
let friend = {
	firstName: "Joe",
	lastName: "Smith",
	address: {
		city: 'Austin',
		state: 'Texas'
	},
	emails: ['joe@mail.com', 'joesmith@email.xyz'],
	introduce: function(){
		console.log("Hello my name is " + this.firstName + ' ' + this.lastName);
	}
};

friend.introduce();





// [section] real application of objects
/*
-scenarion
	1. we would like tp create a game that would have several pokemon intract with each other
	2. every pokemon would have the same set of stats, properties and functions

*/
// Using object literals to create multiple kinds of pokemon be time consuming

let myPokemon = {
	name: 'pikachu',
	level: 3,
	health : 100,
	attack: 50,
	tackle:function(){
		console.log("this  pokemon tackled target pokemon")
		console.log("target pokemons health is now reduced to _targetPokemonHealth_")
	},
	faint:function(){
		console.log('pokemon fainted')
	}
}
console.log(myPokemon)

// creating an object constructor instead will help with the process

function Pokemon(name, level){
	// properties
	  this.name = name;
	  this.level = level;
	  this.health = 2*level;
	  this.attack = level;

	// methods

	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log("targetPokemons health is reduced to _targetPokemonhealth_")
	}
	this.faint =function(){
		console.log(this.name + 'fainted')
	}
}

// Creates new instances of the new "pokemon" object each with their unique properties"

let pikachu = new Pokemon(" Pikachu", 16)
let rattata = new Pokemon( 'Rattata', 8)
co


pikachu.tackle(rattata)