console.log("hello world")

// console.log("Hello World");

//Follow the property names and spelling given in the google slide instructions.

// Create an object called trainer using object literals
let trainer ={
	name: "Ash Ketchum",
	age: 10,
	Pokemon:["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		Hoenn:["Max", "May"],
		Kanto:['Brock', 'Misty']
	},
	talk: function(){
		console.log("Pikachu! I choose you")
	}


}
console.log(trainer);
console.log("Results of dot notation:");
console.log(trainer.name);
console.log("Results of square bracket notation:");
console.log(trainer['Pokemon']);
console.log("Results of talk method:");
trainer.talk();

// Initialize/add the given object properties and methods

// Properties

// Methods

// Check if all properties and methods were properly added


// Access object properties using dot notation

// Access object properties using square bracket notation

// Access the trainer "talk" method


// Create a constructor function called Pokemon for creating a pokemon
function Pokemon(name, level){
	this.name = name;
	this.level = level;
	this.health = 2*level
	this.attack = level


	this.tackle = function(target){

		target.health = target.health- this.attack
		console.log(this.name + ' tackled ' + target.name)
		console.log(target.name +' health is reduced to ' + target.health)

	}  
	this.faint = function(target){
		target.health = target.health- this.attack

			if(target.health <= 0)
		console.log(target.name + " has fainted ")

	}


}

let pikachu = new Pokemon( 'Pikachu', 12);
let geodude = new Pokemon ('Geodude', 8);
let mewtwo = new Pokemon ( 'Mewtwo', 100);

// Create/instantiate a new pokemon
console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu)
geodude.faint(pikachu)
console.log(pikachu)

mewtwo.tackle(geodude)
mewtwo.faint(geodude)
console.log(geodude)




// Create/instantiate a new pokemon


// Create/instantiate a new pokemon


// Invoke the tackle method and target a different object


// Invoke the tackle method and target a different object








//Do not modify
//For exporting to test.js
try{
	module.exports = {
		trainer,
		Pokemon 
	}
} catch(err) {

}