console.log("hello world");

// [section] Getting all post

// The fetch API allows you to asychronously request for a resorces or data
// a promise is an object thet represent the eventual completion(or failure) of an asynchronous function and its value

console.log(fetch('https://jsonplaceholder.typicode.com/posts'));

// retrieve all post following the REST API
// by using the then method we can now check for the status of the promise
fetch('https://jsonplaceholder.typicode.com/posts')
// the 'fetch' method will return a promise that resolves to a response object
// the 'then' method captures the response object and returns another promise which will be eventually resolve or rejected
.then(response=>console.log(response.status));

fetch('https://jsonplaceholder.typicode.com/posts')
 // error 404 file not found
// use the 'json method' from the 'response' object to convert the data retrieved into JSON format to be used in our application
.then((response)=>response.json())
// print converted

.then((json)=>console.log(json))


// the 'async' and 'await' keywords is another approach that can be used to achieve asunchronous code
// used in functions to indicate which portions of code should be waited for
// creates an asynchronous function
async function fetchData(){
	// waits for the fetch method to complete then stores the value in the result variable
let result =await fetch('https://jsonplaceholder.typicode.com/posts')
// result returned by fetch returns a promise
console.log(result)
// return response is an object
console.log(typeof result)
// we cannot access the content of response by directly accessing its body property
console.log(result.body);
// converts the data from the 'response' object as JSON
let json = await result.json()
// print out the content of the 'response'
console.log(json)
}
fetchData();

// [section] getting a specific post
fetch('https://jsonplaceholder.typicode.com/posts/1')
.then((response)=>response.json())
.then((json)=>console.log(json))


// [section]Creating a post

fetch('https://jsonplaceholder.typicode.com/posts',{
	// sets the method of the request object to the POST following the REST API
	// default method is get
	method: 'POST',
	// sets the header data of the request object to be sent to the back end
	// specify that the content will be in a json
	headers:{
		'Content-type': 'application/json',
	},
	// sets the content/body data of the 'request' object to be sent to the backend
	// JSON>stringify convetsnthe objects data into a stringified field
	body:JSON.stringify({
		title: 'New Post',
		body: 'hello world',
		userId: 1

	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));

// [Section] updating a post using 'PUT' method

fetch('https://jsonplaceholder.typicode.com/posts/1',{
	method: 'PUT',
	headers:{
		'Content-type': 'application/json',
	},
	body:JSON.stringify({
		id: 1,
		title:'Updated Post',
		body: 'Hello again',
		userId: 1

	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


// [section] Updating a post using PATCH
// Update a specific post following the REST API
// the difference between PUT and PATCH is the number of properties being changed
// Patch is used to update the whole object
// put is used to update a single/seral properties

fetch('https://jsonplaceholder.typicode.com/posts/1',
{
	method: 'PUT',
	headers:{
		'Content-type': 'application/json',
	},
	body:JSON.stringify({
		title:'Corrected Post',
		
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


// [Section] Deleting a post

// Deleting a specific post following the REST API

fetch('https://jsonplaceholder.typicode.com/posts/1',
{
	method: 'DELETE',
			
})



// [section] Filtering POst

// the data can be filtered by sending the userID along with the URL
// information sent via the url can be done by adding the question mark symbol(?)

fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response)=>response.json())
.then((json)=>console.log(json));

// [section] retrieving nested/related comments to post

// retreiving comments for a specific post following the REST API

fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response)=>response.json())
.then((json)=>console.log(json));