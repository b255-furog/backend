console.log("hello world")



fetch('https://jsonplaceholder.typicode.com/todos')
.then((response)=>response.json())
.then((json)=>console.log(json.map((item)=>{return item.title})));


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response)=>response.json())
.then((json)=>console.log(json))

fetch("https://jsonplaceholder.typicode.com/todos",{
	
	method: "POST",
	
	headers:{
		"Content-type": "application/json",
	},
	
	body:JSON.stringify({
		complete: false,
		title: "Created To Do List Items",
		userId: 1

	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/1",{
	method: "PUT",
	headers:{
		'Content-type': 'application/json',
	},
	body:JSON.stringify({
		
		userId: 1,
		id: 1,
		title:"Updated to do list items",
		completed: "pending",
		description: "To update the my to do list with a different data structure",
		dateCompleted: "Pending"

	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));



fetch("https://jsonplaceholder.typicode.com/todos/1/",
{
	method: "PUT",
	headers:{
		"Content-type": "application/json",
	},
	body:JSON.stringify({

		userId: 1,
		id: 3,
		title:"delectus aut autem",
		completed: false,
		status: "complete",
		dateCompleted: "03-27-2023"
		
	})
})
.then((response)=>response.json())
.then((json)=>console.log(json));


fetch("https://jsonplaceholder.typicode.com/todos/2",
{
	method:"DELETE",
			
})


