let http = require("http")


http.createServer(function(request,response){


	
	if(request.url == "/Homepage" && request.method =="GET"){

	
		response.writeHead(200,{'Content-Type': 'text/plain'});
		response.end('Welcome to the Booking System');
		
	}

	if (request.url =='/profile' && request.method=="GET"){
			response.writeHead(200,{'Content-Type': 'text/plain'});
		
			response.end('Welcome to your Profile!');
	}

	if (request.url =='/courses' && request.method=="GET"){
			response.writeHead(200,{'Content-Type': 'text/plain'});
		
			response.end('Here is our available courses');
	}

	if (request.url =='/addCourses' && request.method=="POST"){
			response.writeHead(200,{'Content-Type': 'text/plain'});
		
			response.end('Add course to our resources');
	}

	if (request.url =='/updateCourses' && request.method=="PUT"){
			response.writeHead(200,{'Content-Type': 'text/plain'});
		
			response.end('Update a course to our resources');
	}

	if (request.url =='/archiveCourses' && request.method=="DELETE"){
			response.writeHead(200,{'Content-Type': 'text/plain'});
		
			response.end('Archives courses to our resources');
	}




}).listen(4000)

console.log("server is running at localhost:4000")