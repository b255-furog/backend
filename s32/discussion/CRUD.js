
let http =require("http")

// mock database

let directory =[
{
	'name': "brandon",
	'email': "brandonemail.com"
},
{
	'name': "Jobert",
	'email': "jobertnemail.com"
}
]

http.createServer(function(request,response){

	// route for returning all items upon receiving of a "GET" request
	if (request.url== "/users" && request.method =="GET"){

		// sets response putput to JSON data type
		response.writeHead(200,{'Content-Type': 'application/json'});

		// inputs has to be data type String hence the JSON.Stringify() method 
		// this string input will be converted to desired output data type which has seen sent to JSON
		// This is done because request and response sent between client and Node.Js server requires the information sent and received as a stringified JSON

		response.write(JSON.stringify(directory));
		response.end()
	}

	// a request object contain several parts
	// -Headers- contain information about the request concept/cpntent like what is the data type
	// Body- contains the actual information being sent with the request
	if (request.url == "/users" && request.method == "POST"){

		let requestBody = "";
// data is received from the client and is processed in the data stream
// the information provided from the request object enters a sequene object enters a sequence called "data the code below will be triggered"
		request.on('data', function(data){

			// assign the data retrieved from the data stream to the requestBody
			requestBody += data
		})


		request.on('end',function(){
			// we need this to be of data type JSON to access its properties
			console.log(typeof requestBody)
			// converts the string requestody to JSON
			requestBody =JSON.parse(requestBody)

			// create a new object representing the new mock ddatabase record

			let newUser ={
				'name': requestBody.name,
				'email': requestBody.email


		}

		directory.push(newUser);
		console.log(directory);
		response.writeHead(200,{'Content-Type': 'application/json'})
		response.write(JSON.stringify(newUser));
		response.end()

		})
			
	}

}).listen(4000)
console.log("server running at localhost:4000")