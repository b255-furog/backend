const express = require("express");
const router = express.Router();
const userController = require("../controllers/user");
const auth = require("../auth");



// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
});

// Route for user authentication
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
});


// Route for Non-Admin User checkout(Create Order)
router.post("/createOrder", auth.verify, (req, res) => {

let data = {
	// User ID will be retrieved from the request header
	userId : auth.decode(req.headers.authorization).id,
	// Course ID will be retrieved from the request body
	productId : req.body.productId
}

	userController.createOrder(data).then(resultFromController => res.send(resultFromController));

});

// Route for retrieving user details
router.get("/details", auth.verify, (req, res) => {
	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userData = auth.decode(req.headers.authorization);
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));

});

// Route to setting user as admin(false to true)
// A "PUT" request is used in activating and make the products available to our users.
router.put("/:userId/admin", auth.verify, (req, res) => {
	// using authority to verify admin only limitation in changing user status
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin ==true){
		console.log(req.body)
		userController.activateUser(req.params).then(resultFromController => res.send(resultFromController));
	}
	
});

// Retrieve all orders(Admin Only)
router.get("/allOrders",auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	if(userData.isAdmin == true){
		console.log(req.body)
		userController.getAllOrders().then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
	
});

// Allows us to export the "router" object that will be acessed in our "index.js" file
module.exports = router;
