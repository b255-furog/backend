const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route for creating a product(Admin Only)
router.post("/", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.addProduct(req.body).then(resultFromController =>
			res.send(resultFromController))
	} else{
		res.send(false);
	}
});

// Route for retrieving all the products
router.get("/all", (req, res) => {
	productController.getAllProducts().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving all the ACTIVE products
// Middleware for verifying JWT is not required because users who are not logged in should also be able to view the products
router.get("/", (req, res) => {
	productController.getAllActive().then(resultFromController => res.send(resultFromController))
});

// Route for retrieving a single product
// Creating a route using the "/:parameterName" creates a dynamic route, meaning the url is not static and chages depending upon the information provided
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	// Since the product ID will be sent via the URL, we cannot retrieve it from the request body
	// We can however retrieve the product ID by accessing the request's "params" property which contains all parameters provided via the url
	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
})

// Route for updating a product(admin only)
// JWT Verification is needed for this route to ensure that a user is logged in before updating a product
router.put("/:productId", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		console.log(req.body)
		productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController))
	}
	
})

// Route to archiving a Product
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the products from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:productId/archive", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin == true){
		productController.archiveProduct(req.params).then(resultFromController => res.send(resultFromController))
	}
	;
});


// Route to activating a Product
// A "PUT" request is used in activating and make the products available to our users.
router.put("/:productId/activate", auth.verify, (req, res) => {
	productController.activateProduct(req.params).then(resultFromController => res.send(resultFromController));
});
// Route for retrieving all orders(admin only)
router.get("/allAdmin", auth.verify,(req, res) => {
	const userData =auth.decode(req.headers.authorization)
	if(userData.isAdmin== true){
		console.log(req.body)
		productController.getAllProductsAdmin().then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}
	
});
// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router;
