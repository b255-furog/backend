// "require" directive-used to load node.js modules
// "http module"-let the node.js transfer data using the HTTP
// "HTTP"-protocol that allows the fetching of resources

/*
we are now able to run a simple node.js server.wherein when we added our URL in the browser, a client actually requested to our server was able to respond with a text.

we used require() method to load node.js modules
	a module is a software component or part of a program which contains one or more routines

	the http module is a default module from node.js

	the http protocol let node.js transfer data or let our client and server exchange data via hypertext transfer protocol

*/

let http=require("http")

/*
http.createServer() method allows us to create a server and handle the request of a client 

request -messages sent by the client, usually via web browser


response  -messages sent by the server as an answer

*/

http.createServer(function(request,response){

	// res.writeHead is a method of the response object.this will allow us to add headers, which are additional information about our servers response.'Content-Type' is one of the more recognizable headers,it is pertaining to the data type of the content we are responding with. The first argument in the writeHead is an HTTP which is used to tell the client about the status of their request. 200 meaning OK. HTTP 404 means the resource you are trying to access cannot be found. 403 means the resource you are trying to access is forbidden or require authentication
	response.writeHead(200,{"Content-Type": "text/plain"});

	// res.end() is a method of the response object which ends the servers response and end a message/data as a string

	response.end("hello world");


	// .listen- allow us to assign a port to a server.
	// a port is virtual point where connections starts and end
	// http://localhost:4000-localhost is your current device/machine and 4000 is the port number assigned to where the process 
	// server is listening or running from.port 4000-popularly used for backend application
}).listen(4000)

// when server is running, console will print out the message;
console.log("Server is running at localhost:4000")