const http = require('http');

// create a variable "port" to sstore the port number
const port = 4000;

// create a variable server that stores the output of the create server method
const server =http.createServer((request,response)=>{
	if(request.url =='/greeting'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('Hello Again')
	} else if (request.url =='/Homepage'){
		response.writeHead(200, {'Content-Type': 'text/plain'})

		response.end('This is the Homepage')
	}else{
		response.writeHead(404, {'Content-Type': 'text/plain'})

		response.end('404-Page not available')
	}
});

// uses the "server" and "port" variables created above
server.listen(port)

console.log(`Server now accessible at localhost:${port}`);

