console.log("hello world");

// section while loops

/*
	- a while loops takes ain an expresion/condition
	-exprression are any unit of code that can be evaluated to a value
	-if the the condition evaluates to tru, the statments inside the code block will be evalauted
	-a tatement is a command the programmer gives to the computer
	-a loop will iterate a certain number of times until an expression/condition is met


*/

let count = 5;


// while the value of count is not equal to 0
while(count !== 0){

	// the current value of the count is printed out
	console.log("while: " + count);

	// decreases the value of count by 1 after every iteration to stop the loop when it reaches 0
	// Mkake sure that expression/condition in loops have their corresponding increment/decrement operators to stop the loop
	count --;
}

// section -do while loops

/*/
	-a do while loops works a lot like a while loop but  unlike while loops, do while loops guarantee a code will be executed at least once.



*/

// let number = Number(prompt("Give me a number:"))

// do {
// 	console.log("Do while: " + number)
// 	number +=1
// } while(number < 10)

// [Section] for loops

/*
	- a for loop is more flexible than while and do while loops.
	1. the initialization value that will track the progress of the loop
	2. the expression/condition that will be evaluated which will determine whether the loop will run one more time
	3. the final expression indicates how to advance tyhe loop

*/
// for( let count = 0; count <= 20; count++){
// 	console.log(count)
// }

let myString ="alex"
// charater in strings may be counted using the .length propeerty
console.log(myString.length)
// accessing elements of a string

console.log(myString[0])
console.log(myString[1])
console.log(myString[2])

for (let x = 0; x < myString.length; x++){
	console.log(myString[x])
}
// write a for loop that will print numbers from 1 to 10
// it must be inside a function

function myNumbers(){
	for(let x =0; x<=10; x++){
		console.log(myNumbers)
	}

}
myNumbers()

let myName = "Alex"

for (let i = 0; i < myName.length; i++){
	console.log(myName[i])
}

if(
	myName[i].toLowercase() == "a" ||
	myName[i].toLowercase() == "e" ||
	myName[i].toLowercase() == "i" ||
	myName[i].toLowercase() == "0" ||
	myName[i].toLowercase() == "u"
){
  console.log(3)
}else{
	console.log(myName[i])
}
