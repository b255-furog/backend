// [section] syntax ,statment and comments
// statements in programming are instruction thta we tell the computer to perform
// JS statement usually ends with a semi colon(;)
console.log("Hello World");

// comments are parts of the code that gets ignored by the language
// comments are meant to describe the written code

/*There are 2 types of comment:
1. The single line comment denoted by two slashes
2. The multiline comment denoted by a slash and asterisk*/

// [section] Variables
// it is used to contain data
// any information by an application is stored in what we called memory
// when we create variables certain portion of a devices memory is given name that we call variable

// declaring variables
// declaring variables tell our device that a variable name is created and ready to store data
// declaring a variable without assigning a value will automatically give it the value of undefined"
 let myVariable;
 console.log(myVariable);
 // console.log is useful for printing out values of variables or certain results of code into the console
 let hello;
 console.log(hello);

 // Variables must be declared first before they are used
 // using variaables before they're declared will return an error.

 /*Gude in writing variables
 1. use the let keyword followed by the variable name of your choosing and use the assignment operator
 (==) to assign a value
 2. variable names shouldstart with lower character, use camelcase for multiple keywords
 3. for constant variables, use the conts keyword
 4. variables names should be indicative (or descriptive) of the value stored to avoid confusion
 */


// declaring and initializing variable
// initializing variables-the instance when a variable when given its




let productName = 'desktop computer';
console.log(productName);

let productPrice = 18999;
console.log(productPrice);

// In the context of certain appliocation, some variables information are constant and should not be changed
// in this example the interest rate for a loan, savings account or a mortgage must not changed due to real world concerns.


const interest = 3.539;

// reassigning variable values
// reassigning a varable means changing its initial or previous value into another value

productName = "laptop";
console.log(productName);

// trying to reassigned

productName = "oculus";
console.log(productName);


// reassigning var vs initializing variables
// declare a variable first

let supplier;
// initialization is done after ythe variable has been declared
// this value is considered as initialization because itis the first time that value has been assigned to a varialble

supplier = "johnsmith trading";
console.log(supplier);
 // this is considereda reaassignment becoz initial value was already declared
supplier ="zuitt store"
console.log(supplier)

// var vs. let/const

// var-is also used in declaring variable but var is an ECMAScript(ES!) feature (javascript 1997)
// while let and const as a new feature in ES6

// there are issues associated with variable declared with var, regarding hoisting
// hoisting is javascripit default behavior of moving declarations to the top
// in terms of variable and constant, keyword var is hoisted and let const does not allow hoisting

// e.g
a = 5;
console.log(a);
var a;

// in the above ex., variable 'a' is used before declaring it and the program works and displays the out of 5.
// let/const/local/global scope
// scope is essentially means where thes variable are available for use
// let and const are block scope
// a block is a chunk of code bounded by{} a blocks lives in
// curly braces. anythinmg withiin curly braces is a block

// multiple var declaration
// multiple var mayble declared in 1 line
// though it is quicker w/o having to retype the "let" keyword, it is still best practice to use multiple let/const

let productCode ='DC017', productBrand='dell';
console.log(productCode, productBrand);

// data types
// section-data types

// strings
// are a series of character that are created a word, a phrase, a sentaence, or anything related to creating text
// strings in js can be written usuing either singl('') or double ("") qoute

let country ="Phil";
let province ="Memtro manila";

// Concatenating strings
// multiple string values can be combined to create a single string using the plu(+) symbol

let fullAddress = province + ',' +country;
console.log(fullAddress);

let greeting = 'I live in the phil' + country;
console.log(greeting);

// \n refers to a creatinga new line in between text

let mailAddress = "Metro Manila \n\n Philippines";
console.log(mailAddress);

let message = "john's employee went home early";
console.log(message);

message = 'john \'s employees went home early';
console.log(message)

// numbers
// integers/whole numbers

let headCount = 26;
console.log(headCount);

// decimals numbers/fraction

let grade = 98.7;
console.log(grade);

// exponentials notation

let planetDistance = 2e10;
console.log(planetDistance);

// combining text and strings
console.log("john's grade last quarter is " + grade);


// bolean
// bolean values are normally used to store values relating to the state of certain things
// this will be usueful in further discusions about creating logic to make our applications respond to certain scenarios

let isMarried = false;
let inGoodConduct = true;
console	.log("isMarried: " + isMarried)


//Arrays are special kind of data types that used to store multiple values
// arrays can store different data types but is norna;;y used to store similar data types
let grades = [98.7, 92.5, 90.2, 90.5];
console.log(grades);

// index strat at 0

// diff dat types

// storing diff dat types inside an array is not recommened becaus eit will not make sense in the next ocntent of programming

let details = [ "john", "smith", 32, true];
console.log(details)

// object
// are another special kind of dat types that used to mimic real worl objects/items
// they are used to craete complex data taht containes pieces of information thaty are relevant to each other
// every individula piece of information is called property of the object

let person = {
	fullName: "jhon dela cruz",
	age: 35,
	isMarried: false,
	contact: [ "09176311879", '091763225'],
	address: {
		houseNumber: "345",
		city: "manila"
	}

}
console.log(person);

let myGrades = {
	firststGrading: 98.7,
	secondGrading: 90.2,
	thirdGrading: 90.1,
	fourthGrading:64.5
}
console.log(myGrades);

// typeof operator is used to

console.log(typeof myGrades)

const anime =[ "one punch", "one piece", "attack titen"];
console.log(anime[0]);

anime[0] = "kimetsu no yaiba";
console.log(anime);

// null
// it is used to intentionally to express the absence of a value in a variable declaration/initialization

let spouse = null;

// using null compared to a zero is much better for readability purposes
// null is also considered to be a data type of its own compared to zero which is a data type of number

// undefine
// repsesnt the state of a variable that has beeen declared but  without assigned value

let fullName;
console	.log(fullName);

// undefined vs. Null
// one clear diff bet a null and undefinied is that for undefined , a vaia;be sas created but was not provided a value
// null means that a variable was created