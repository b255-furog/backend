const express = require("express")
const router = express.Router()
const courseController = require("../controllers/course")
const auth = require("../auth")
// Route for creating a course


/*
	router.get("/details", auth.verify, (req, res) => {
	// Uses the "decode" method defined in the "auth.js" file to retrieve user information from the token passing the "token" from the request header as an argument
	const userDate = auth.decode(req.headers.authorization)
	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : req.body.id}).then(resultFromController => res.send(resultFromController));
});
*/


// router.post("/", (req,res)=>{
	// courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
// });



router.post("/", auth.verify, (req,res)=>{

	const userData = auth.decode(req.headers.authorization);
	if(userData.isAdmin ==true){
		console.log(req.body)
		courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController))
	}else{
		res.send(false)
	}

	
})

// Routes for retrieving all the courses

router.get("/all",(req,res)=>{
	courseController.getAllCourses().then(resultFromController =>res.send(resultFromController))
})

// Routes for retrieving all the active courses
// Middleware for verifying JWT is not required because users whoa are not logged in should also be able to view the courses
router.get("/",(req,res)=>{
	courseController.getAllActive().then(resultFromController =>res.send(resultFromController))
})

// Routes for retrieving a specific course
// Creating a route using "/:parameterName" creates a dynamic route, meaning the url is not static and changes depending upon the infromation provided

router.get("/:courseId",(req,res)=>{
	console.log(req.params.courseId)
	// since the course ID will be sent via the URL, we cannot retrieve it from the request body
	// WE can however retrieve the course ID by accessing the request's "params" property which contains all paramters provided via the url
	courseController.getCourse(req.params).then(resultFromController =>res.send(resultFromController))
})

// Route for updating a course
// JWT Verification is needed for this route to ensure that a user is logged in before updating a course

router.put("/:courseId", auth.verify,(req,res)=>{
	courseController.updateCourse(req.params,req.body).then(resultFromController=> res.send(resultFromController))
})



// S40 ACTIVITY
// Route to archiving a course
// A "PUT" request is used instead of "DELETE" request because of our approach in archiving and hiding the courses from our users by "soft deleting" records instead of "hard deleting" records which removes them permanently from our databases
router.put("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});


// router.put("/:courseId/archive", auth.verify,(req,res)=>{
// 	courseController.archiveCourse({courseId:req.params.courseId},req.body).then(resultFromController=> res.send(resultFromController))
// })





// Allows us to export the "router" object that will be accessed in our "index.js" file
module.exports = router