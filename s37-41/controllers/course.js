const Course = require("../models/Course")
const auth = require("../auth");
const User = require("../models/User")
// Create a new course
/*
steps
1. Create a new course object  using the mongoose model and the information from the request body and the id from the header
2. Save the new Course to the database

*/
module.exports.addCourse = (reqBody)=>{
	// Create a variable "newCourse" and instantiate a new "Course" object using the mongoose model

	let newCourse = new Course({
		name:reqBody.name,
		description:reqBody.description,
		price: reqBody.price


	})

	let newUser = new User({
		isAdmin:true
	})
	// Saves the created object to our database
	return newCourse.save().then((course,error)=>{
		// Course creation failed
		if(error){
			return false

			 // return newCourse.save(user.isAdmin).then((result,error)=>{
		 	// if(result.isAdmin !== true){
		 	// 	console.log(error)
		 	// 	return true
		 	// 	 return result
		 // 	}
		 // })
			// Course creation successful
		}else{
			return true
		}

		 // return NewCourse.save().then((user,error)=>{
		 // 	if(user.isAdmin !== true){
		 // 		return true
		 // 	}else{
			// 	return false
		 // 	}
		 // })
		
	})
	

}

// Retrieve All Courses
/*
	Steps:
	1. Retrieve all the courses from the database

*/
module.exports.getAllCourses =()=>{
	return Course.find({}).then(result=>{
		return result;
	})
}

// Retrieve all active courses
/*
	Steps:
	1. Retrieve all the active courses from the database with the property of "isActive" to true

*/

module.exports.getAllActive =()=>{
	return Course.find({isActive : true}).then(result=>{
		return result;
	})
}

// Retrieving a specific course
/*
	Steps:
	1. Retrieve the course that matches the course ID provided from the URL
*/
module.exports.getCourse =(reqParams)=>{
	return Course.findById(reqParams.courseId).then(result=>{
		return result
	})
}

// Update a course
/*
	steps:
	1.Create a variable "updateCourse" which will the contain information retrieved from the request body
	2. Find and update the course using the course ID retrievd from the params property and the variable "updateCourse" containing the information from the request body

*/

// Information to update a course will be coming from both the URL paramters and the request
module.exports.updateCourse = (reqParams, reqBody)=>{

	// Specify the field/properties of the document to be updated
	let updateCourse = {
		name:reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
	return Course.findByIdAndUpdate(reqParams.courseId, updateCourse).then((course,error)=>{
		// Course is not updated
		if(error){
			return false
			// Course Updated sucessfully
		}else{
			return true
		}
	})
}

// Archive a course
// In managing databases, it's common practice to soft delete our records and what we would implement in the "delete" operation of our application
// The "soft delete" happens here by simply updating the course "isActive" status into "false" which will no longer be displayed in the frontend application whenever all active courses are retrieved
// This allows us access to these records for future use and hides them away from users in our frontend application
// There are instances where hard deleting records is required to maintain the records and clean our databases
// The use of "hard delete" refers to removing records from our database permanently

module.exports.archiveCourse = (reqParams) => {

	let updateActiveField = {
		isActive : false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {

		// Course not archived
		if (error) {

			return false;

		// Course archived successfully
		} else {

			return true;

		}

	});
};

 // module.exports.archiveCourse = (reqParams, reqBody)=>{

	
 // 	let archiveCourse = {
 // 		name:reqBody.name,
 // 		descrition:reqBody.description,
 // 		price:reqBody.price,
 // 		isActive : reqBody.isActive
 // 	}

 // 	// Specify the field/properties of the document to be updated
	
 // 	return Course.findByIdAndUpdate(reqParams.courseId,archiveCourse).then((course,error)=>{
 // 		// Course status is not updated
 // 		if(error){
 // 			return false;
 // 			// Course Status Updated sucessfully updated
 // 		}else{
 // 			return true
 // 		}


 // 	})
 // }
// module.exports.updateCourseStatus = (CourseId) => {
//    	return Task.findById(CourseId).then((result, error) => {
//    		if(error){
//    			console.log(error);
//    			return false;
//    		}
//    			result.status = "complete"
//    		return result.save().then((updateCourseStatus, saveErr) => {
//    			if(saveErr){
//    				console.log(saveErr);
//  	 			return false;
//    			} else {
//    				return updateTask;
//    			}
//    		})
//    	})
//    }