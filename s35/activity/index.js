const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 4000;

mongoose.connect("mongodb+srv://edison-255:admin123@zuitt-bootcamp.rrtfy74.mongodb.net/?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology: true
	}
);


let db = mongoose.connection

db.on("error", console.error.bind(console, "connection error"));

db.once("open", () => console.log("We're connected to the cloud database"))


const taskSchema = new mongoose.Schema({
	
	username: String,
	password: String,
	status: {}
	
})



const Task = mongoose.model("Task", taskSchema);


app.use(express.json());

app.use(express.urlencoded({extended:true}));

app.post("/signup", (req, res)=> {
	
	
	Task.findOne({username : req.body.username, password : req.body.password}).then((result, err) => {
		
		if(result != null && result.username == req.body.username && result.password == req.body.password){
			
			return res.send("Duplicate username found");

		
		} else {

			
			let newTask = new Task({
				username : req.body.username,
				password : req.body.password
			});

			
			newTask.save().then((savedTask, saveErr) => {
				
				if(saveErr){

					
					return console.error(saveErr);

				
				} else {

					
					return res.status(201).send("New user registered");

				}
			})
		}

	})
})







if(require.main === module){
	app.listen(port, () => console.log(`Server running at port ${port}`));

}

module.exports = app;