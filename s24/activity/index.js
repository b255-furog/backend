console.log('hello world')
// Exponent Operator
let getCube = 2 ** 3;
console.log(`The cube of 2 is ${getCube}`);
// Template Literals

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
const[houseNumber, street, state, zipCode] = address;

console.log(`I live at ${houseNumber} ${street} ${state} ${zipCode} .`);
// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}
const{name, species, weight, measurement} = animal;
console.log(`${name} was a ${species}. It weighs ${weight} and measured ${measurement} .`)

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];
numbers.forEach((numbers) =>{
	console.log(`${numbers}`)
});

 const add = (a,b,c,d,e) => a+b+c+d+e;
 let reducedNumber = add(1,2,3,4,5);
 console.log(reducedNumber)

// Javascript Classes
class dog{
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const myDog = new dog();

myDog.name = "Aspin";
myDog.age = "2 years old";
myDog.breed =" Asong Kalye"	;

console.log(myDog)
